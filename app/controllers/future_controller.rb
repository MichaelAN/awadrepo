class FutureController < ApplicationController

require 'open-uri'

	def index
		url= ["http://hatrafficinfo.dft.gov.uk/feeds/datex/England/FuturePlanned/content.xml","http://hatrafficinfo.dft.gov.uk/feeds/datex/England/FutureRoadworks/content.xml"]
		array1=[]
		array2=[]
		array3=[]
		array4=[]
		url.each do |set|
		@doc = Nokogiri::HTML(open(set))
		#doc = Nokogiri::XML(open(set))
		@doc.xpath("//comment/value").each do |link|
		result = (link.to_s).gsub!(/<value lang="en">/, "")
		array1.push(result.gsub!(/<\/value>/, ""))
		end
		@doc.xpath("//delays").each do |link|
		result = (link.to_s).gsub!(/<delays>/, "")
		result = result.gsub!(/<\/delays>/, "")
		result = result.gsub!(/<delaytimevalue>/, "")
		result = result.gsub!(/<\/delaytimevalue>/, "")
		array2.push((result.to_i)/60)
		end
		@doc.xpath("//validperiod/startofperiod").each do |link|
		result = (link.to_s).gsub(/<startofperiod>/, " From ")
		result = result.gsub(/<\/startofperiod>/, "")
		result = result.gsub(/T/, "  ")
		result = result.gsub(/[Z]/, "")
		array3.push(result)
		end
		@doc.xpath("//validperiod/endofperiod").each do |link|
		result = (link.to_s).gsub(/<endofperiod>/, "")
		result = result.gsub(/<\/endofperiod>/, "")
		result = result.gsub(/T/, "  ")
		result = result.gsub(/[Z]/, "")
		array4.push(result)
		end
		end
		@description = array1
		@delay = array2
		@period = array3
		@end = array4
		if session[:user_id] != nil
			@user = User.find(session[:user_id]).email
		else
			@user = "Guest"
		end
	end
	def search
		url= ["http://hatrafficinfo.dft.gov.uk/feeds/datex/England/FuturePlanned/content.xml","http://hatrafficinfo.dft.gov.uk/feeds/datex/England/FutureRoadworks/content.xml"]
		array1=[]
		url.each do |set|
		@doc = Nokogiri::HTML(open(set))
		@doc.xpath("//comment/value").each do |link|
		result = (link.to_s).gsub!(/<value lang="en">/, "")
		result = result.gsub!(/<\/value>/, "")
		if result.include?(params[:key]) == true
			array1.push(result)
		end
		end
		end
		if array1.nil? == true
			@finding = ["There were no results for your query"]
			else
			@finding = array1
		end
				if session[:user_id] != nil
			@user = User.find(session[:user_id]).email
		else
			@user = "Guest"
		end
	end
end